const int edge = 10;
const long d = 10;
const int analogInPin = A0;
const int audioOutPin = 12;

void setup() {
  //Serial.begin(115200);
}

void loop() {
  long freq = measureFrequency();
  if (freq != 0)
    tone(audioOutPin, freq);
  else 
    noTone(audioOutPin);
  //Serial.println(freq);
}

long measureFrequency() {
  long t = millis();
  long n = 0;
  boolean o = analogRead(analogInPin) <= edge;

  while(millis() - t <= d)
    if ((analogRead(analogInPin) <= edge) != o) {
      o = !o;
      n++;
    }
  return n * 1000 / d;
}




